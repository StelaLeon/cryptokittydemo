
lazy val commonDependencies = Seq(
  "com.lightbend" %% "kafka-streams-scala" % "0.2.1",
  "io.spray" %% "spray-json" % "1.3.5",
  "com.github.pureconfig" %% "pureconfig" % "0.9.2",
  "org.scalamock" %% "scalamock-scalatest-support" % "3.6.0" % Test,
  "org.scalatest"     %% "scalatest"            % "3.0.5"         % Test,
  "org.apache.kafka" % "kafka-streams-test-utils" % "1.1.1" % Test // THIS VERSION

)

lazy val pricehistory = project
  .in(file("."))
  .settings(
    inThisBuild(List(
      scalaVersion := sys.env.get("SCALA_VERSION").getOrElse("2.12.6")
    )),
    name := "fancyKafkaStreams",
    libraryDependencies ++= commonDependencies
  )
