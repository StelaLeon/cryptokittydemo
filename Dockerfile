FROM ubuntu:bionic

RUN apt update
RUN apt install wget python3 librdkafka-dev python3-pip -y
RUN pip3 install confluent-kafka

RUN wget https://s3.eu-west-2.amazonaws.com/coding-challenge-data-ta/full-transactions-6355001_6357571.json
RUN wget https://s3.eu-west-2.amazonaws.com/coding-challenge-data-ta/producer.py

ENTRYPOINT ["/usr/bin/python3", "producer.py", "kafka", "full-transactions"]