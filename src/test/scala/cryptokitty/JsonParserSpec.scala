package cryptokitty

import org.scalatest.{FlatSpec, Matchers}
import spray.json._
class JsonParserSpec extends FlatSpec with Matchers with JsonSupport {
//make sure the json parsers always work
  
  val fullTransactionString = """{"blockNumber":6355001,
      "minedAt":1537282715000,
      "tx":{
        "blockHash":"0xfba6f47b8f1c9a82440cd623e52547968595dc850522d0cf01c56ca5fca378a1",
        "blockNumber":6355001,
        "fromAddr":"0x1ca84425c579fbf93def1d319272532446b53140",
        "gas":90000,
        "gasPrice":20000000000,
        "hash":"0xc209a9bb2168a49fa84a4cf89995d44bebcde8df13e06e6fb25e21462824037b",
        "input":"0x00",
        "nonce":"0x86",
        "toAddr":"0x876eabf441b2ee5b5b0554fd502a8e0600950cfa",
        "transactionIndex":10,
        "weiValue":4659347698871345152,
        "v":27,
        "r":"0x2a96206385eaec8e15030c5fbb9ee60d30960535441c80c6482a27e341e1814a",
        "s":"0x3190e98d1060bbdb1ec9f4bd29431d16782c14f99736264957b4d38c7804a5d3"
      },
     
      "receipt":{
        "blockHash":"0xfba6f47b8f1c9a82440cd623e52547968595dc850522d0cf01c56ca5fca378a1",
        "blockNumber":6355001,
        "contractAddress":null,
        "cumulativeGasUsed":337242,
        "fromAddr":"0x1ca84425c579fbf93def1d319272532446b53140",
        "gasUsed":21004,
        "logs":[],
        "logsBloom":"0x00000000000",
        "status":1,
        "toAddr":"0x876eabf441b2ee5b5b0554fd502a8e0600950cfa",
        "transactionHash":"0xc209a9bb2168a49fa84a4cf89995d44bebcde8df13e06e6fb25e21462824037b",
        "transactionIndex":10
       }
      }
    """.stripMargin

  "a full transaction" should " parsed correctly with no errors" in{
    val fullTransObj = fullTransactionString.parseJson.convertTo[FullTransaction]
    fullTransObj.blockNumber shouldEqual(6355001)
  }
}
