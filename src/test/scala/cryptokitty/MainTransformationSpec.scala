package cryptokitty

import java.util.Properties

import org.scalatest._
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.TopologyTestDriver
import org.apache.kafka.streams.test.ConsumerRecordFactory
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.kafka.common.serialization.StringDeserializer
import java.util
import java.util.Properties
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.serialization._
import org.apache.kafka.streams.StreamsConfig
import org.apache.kafka.common.serialization.Serdes

class MainTransformationSpec extends FlatSpec with Matchers  with KafkaTopology {

  val fullTransactionString = """{"blockNumber":6355001,
      "minedAt":1537282715000,
      "tx":{
        "blockHash":"0xfba6f47b8f1c9a82440cd623e52547968595dc850522d0cf01c56ca5fca378a1",
        "blockNumber":6355001,
        "fromAddr":"0x1ca84425c579fbf93def1d319272532446b53140",
        "gas":90000,
        "gasPrice":20000000000,
        "hash":"0xc209a9bb2168a49fa84a4cf89995d44bebcde8df13e06e6fb25e21462824037b",
        "input":"0x00",
        "nonce":"0x86",
        "toAddr":"0x876eabf441b2ee5b5b0554fd502a8e0600950cfa",
        "transactionIndex":10,
        "weiValue":4659347698871345152,
        "v":27,
        "r":"0x2a96206385eaec8e15030c5fbb9ee60d30960535441c80c6482a27e341e1814a",
        "s":"0x3190e98d1060bbdb1ec9f4bd29431d16782c14f99736264957b4d38c7804a5d3"
      },

      "receipt":{
        "blockHash":"0xfba6f47b8f1c9a82440cd623e52547968595dc850522d0cf01c56ca5fca378a1",
        "blockNumber":6355001,
        "contractAddress":null,
        "cumulativeGasUsed":337242,
        "fromAddr":"0x1ca84425c579fbf93def1d319272532446b53140",
        "gasUsed":21004,
        "logs":[],
        "logsBloom":"0x00000000000",
        "status":1,
        "toAddr":"0x876eabf441b2ee5b5b0554fd502a8e0600950cfa",
        "transactionHash":"0xc209a9bb2168a49fa84a4cf89995d44bebcde8df13e06e6fb25e21462824037b",
        "transactionIndex":10
       }
      }
                              """.stripMargin.toString()


  "a full transaction string" should " pass and be parsed correctly through the stream" in{

    import org.apache.kafka.streams.StreamsConfig
    val strings = Serdes.String()
    val config = new Properties()
   config.put(StreamsConfig.APPLICATION_ID_CONFIG, "test")
   config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "dummy:1234")
    val testDriver : TopologyTestDriver = new TopologyTestDriver(getKafkaTopology(), config)

    val factory  = new ConsumerRecordFactory("", new StringSerializer(), new StringSerializer())
   testDriver.pipeInput(factory.create("full-transactions", "", fullTransactionString ))

   val outputRecord = testDriver.readOutput("CryptoKitty-tx", new StringDeserializer(), new StringDeserializer());

    outputRecord shouldEqual("")
 }
}
