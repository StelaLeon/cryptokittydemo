package cryptokitty

import spray.json._

case class TX (blockHash: String,
                blockNumber: Long,
                fromAddr:String,
                gas: Long,
                gasPrice: Long,
                hash: String,
                input: String,
                nonce: String,
                toAddr: String,
                transactionIndex: Long,
                weiValue: Long,
                v: Long,
                r: String,
                s: String)

  case class Receipt (blockHash: String,
                blockNumber: Long,
                contractAddress: Option[String],
                fromAddr: String,
                gasUsed: Long,
                logs: Option[Array[String]],
                logsBloom: String,
                status: Long,
                toAddr: String,
                transactionHash: String,
                transactionIndex: Long)


case class FullTransaction(
                            blockNumber: Long,
                            minedAt : Long,
                            tx: TX,
                            receipt:Receipt)

trait JsonSupport  {
  import spray.json.DefaultJsonProtocol._
  implicit val txJFormat = jsonFormat14(TX)
  implicit val receiptJFormat = jsonFormat11(Receipt)

  implicit val searchCriteriaJsonFormat = jsonFormat4(FullTransaction)

  def stringToFullTransaction(fullTransString: String) :FullTransaction = {
    fullTransString.parseJson.convertTo[FullTransaction]
  }


}
