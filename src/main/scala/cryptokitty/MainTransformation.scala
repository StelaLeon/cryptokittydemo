package cryptokitty
import java.util.Properties
import java.util.concurrent.TimeUnit

import org.apache.kafka.streams._
import org.apache.kafka.streams.kstream.{KStream}

object MainTransformation extends App with KafkaTopology {

  val config: Properties = {
    val p = new Properties()
    p.put(StreamsConfig.APPLICATION_ID_CONFIG, "map-function-scala-example")
    val bootstrapServers = if (args.length > 0) args(0) else "localhost:9092"
    p.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers)
    p
  }



  val streams: KafkaStreams = new KafkaStreams(getKafkaTopology, config)
  streams.start()

  sys.ShutdownHookThread {
    streams.close(10, TimeUnit.SECONDS)
  }

}

trait KafkaTopology extends JsonSupport{
  def getKafkaTopology() :Topology = {
    val builder = new StreamsBuilder()
    val transactions: KStream[String, String] = builder.stream[String, String]("full-transactions")

    // Variant 1: using `mapValues`
    val fullTransactionsStram: KStream[String,FullTransaction] = transactions.mapValues[FullTransaction](stringToFullTransaction(_))
      .filter{ case (m: String, el: FullTransaction)=>el.tx.toAddr.equals("0x06012c8cf97bead5deae237070f9587f8e7a266d") }

    //And write all CryptoKitty transactions towards a CryptoKitty-tx topic.
    fullTransactionsStram.to("CryptoKitty-tx")

    builder.build()
  }
}
